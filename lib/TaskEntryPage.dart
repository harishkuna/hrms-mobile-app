import 'dart:convert';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:hrms_ui_app/Global.dart';
import 'package:http/http.dart' as http;

import 'model/Timesheet.dart';

FileType _pickingType;

bool _loadingPath = false, _multiPick = false, _hasValidMime = false;
String _fileName, _extension, _path;
Map<String, String> _paths;
TextEditingController _controller = new TextEditingController();

class TaskEntryPage extends StatefulWidget {
  Timesheet timeSheetObj;
  var selectedProject;
  var selectedTask;
  var selectedDate;

  TaskEntryPage(this.timeSheetObj, this.selectedProject, this.selectedTask,
      this.selectedDate);

  @override
  TaskListPage createState() => TaskListPage();
}

class TaskListPage extends State<TaskEntryPage> {
  List<String> projectNames = new List();
  List<String> taskNames = new List();
  List<bool> isSelected;
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  void initState() {
    for (var i = 0; i < widget.timeSheetObj.entries.length; i++) {
      if (widget.selectedTask == widget.timeSheetObj.entries[i].task.name)
        projectNames.add(widget.timeSheetObj.entries[i].task.project.name);
    }

    print("projectNames is ${projectNames}");
    isSelected = [true, false];
    super.initState();
  }

  void _openFileExplorer() async {
    if (_pickingType != FileType.CUSTOM || _hasValidMime) {
      setState(() => _loadingPath = true);
      try {
        if (_multiPick) {
          _path = null;
          _paths = await FilePicker.getMultiFilePath(
              type: _pickingType, fileExtension: _extension);
        } else {
          _paths = null;
          _path = await FilePicker.getFilePath(
              type: _pickingType, fileExtension: _extension);
        }
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }
      if (!mounted) return;
      setState(() {
        _loadingPath = false;
        _fileName = _path != null
            ? _path.split('/').last
            : _paths != null ? _paths.keys.toString() : '...';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Entry TimeSheet"),
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: new Text('Project Name : ' + widget.selectedProject,
                    style: new TextStyle(fontSize: 18.0)),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: new Text('Task Name : ' + widget.selectedTask,
                    style: new TextStyle(fontSize: 18.0)),
              ),
              Row(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: new Text('Billable',
                        style: new TextStyle(fontSize: 18.0),
                        textAlign: TextAlign.left),
                  ),
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: new ToggleButtons(
                      borderColor: Colors.blue,
                      fillColor: Colors.black,
                      borderWidth: 2,
                      selectedBorderColor: Colors.blue,
                      selectedColor: Colors.blue,
                      borderRadius: BorderRadius.circular(0),
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Yes',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'No',
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ],
                      onPressed: (int index) {
                        setState(() {
                          for (int i = 0; i < isSelected.length; i++) {
                            if (i == index) {
                              isSelected[i] = true;
                            } else {
                              isSelected[i] = false;
                            }
                          }
                        });
                      },
                      isSelected: isSelected,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: new TextField(
                  style: new TextStyle(
                      fontSize: 18.0, height: 2.0, color: Colors.black),
                  decoration: InputDecoration(hintText: "Enter Hours"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20.0, left: 12, right: 12),
                child: new DropdownButton(
                    iconSize: 40,
                    iconDisabledColor: Colors.orange,
                    isExpanded: true,
                    //to fullfill the Dropdownwidth for entire sreen
                    iconEnabledColor: Colors.blueAccent,
//bottom line for DropDownButton purpose
                    underline: Container(
                      decoration: const BoxDecoration(
                          border:
                              Border(bottom: BorderSide(color: Colors.grey))),
                    ),
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                    ),
                    hint: new Text('Upload'),
                    value: _pickingType,
                    items: <DropdownMenuItem>[
                      new DropdownMenuItem(
                        child: new Text('FROM AUDIO'),
                        value: FileType.AUDIO,
                      ),
                      new DropdownMenuItem(
                        child: new Text('FROM IMAGE'),
                        value: FileType.IMAGE,
                      ),
                      new DropdownMenuItem(
                        child: new Text('FROM VIDEO'),
                        value: FileType.VIDEO,
                      ),
                      new DropdownMenuItem(
                        child: new Text('FROM ANY'),
                        value: FileType.ANY,
                      ),
                    ],
                    onChanged: (value) => setState(() {
                          _pickingType = value;
                          _openFileExplorer();
                          if (_pickingType != FileType.CUSTOM) {
                            _controller.text = _extension = '';
                          }
                          print("picking type is ${_pickingType}");
                        })),
              ),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(30.0),
                    color: Color(0xff01A0C7),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        print(widget.timeSheetObj.toJson());
                        main();
                      },
                      child: Text("Submit",
                          textAlign: TextAlign.center,
                          style: style.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.bold)),
                    ),
                  ))
            ]),
      ),
    );
  }

  main() async {
    String url =
        'http://192.168.10.125:8080/timesheet/';
    Map map = widget.timeSheetObj.toJson();

    print(await apiRequest(url, map));
  }

  Future<String> apiRequest(String url, Map jsonMap) async {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(url));
    request.headers.set('content-type', 'application/json');
    request.headers.set(HttpHeaders.AUTHORIZATION, 'Bearer '+Global.userObject.accessToken);
    request.add(utf8.encode(json.encode(jsonMap)));
    HttpClientResponse response = await request.close();
    // todo - you should check the response.statusCode
    String reply = await response.transform(utf8.decoder).join();
    httpClient.close();
    return reply;
  }
}
