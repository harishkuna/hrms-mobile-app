import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:hrms_ui_app/home1.dart';

import 'TaskEntryPage.dart';
import 'model/Timesheet.dart';

class TaskPage extends StatefulWidget {
  Timesheet timeSheetObj;

  TaskPage(this.timeSheetObj);

  @override
  TaskListPage createState() => TaskListPage();
}

class TaskListPage extends State<TaskPage> {
  var selectedProject;
  var selectedTask;
  var selectedDate;

//  List<String> projectNames = new List();
//  List<String> taskNames = new List();

  @override
  void initState() {
//    for (var i = 0; i < widget.timeSheetObj.entries.length; i++) {
//      projectNames.add(widget.timeSheetObj.entries[i].task.project.name);
//    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Task"),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              print("adding new project");
            },
            child: Text("Add Project"),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: Column(children: <Widget>[
          DatePickerTimeline(
            DateTime.now(),
            onDateChange: (date) {
              // New date selected
              print(date.toString());
              selectedDate = date.toString();
            },
          ),
          Expanded(
            child: new ListView.builder(
                padding: EdgeInsets.all(10.0),
                shrinkWrap: true,
                itemCount: widget.timeSheetObj.entries.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    child: ListTile(
                      title: Text('Project Name : ' +
                          widget.timeSheetObj.entries[index].task.project.name),
                      subtitle: Text('Task Name : ' +
                          widget.timeSheetObj.entries[index].task.name),
                      onTap: () {
                        if (selectedDate.toString().length > 0) {
                          selectedProject = widget.timeSheetObj.entries[index].task.project.name;
                          selectedTask = widget.timeSheetObj.entries[index].task.name;
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TaskEntryPage(
                                    widget.timeSheetObj, selectedProject, selectedTask, selectedDate)),
                          );
                        } // If Condition
                      },
                    ),
                  );
                }),
          )
        ]),
      ),
    );
  }
}

/*
DropdownButton<String>(
hint: Text("Select Task"),
value: selectedProject,
isDense: true,
items: projectNames.map((String dropDownStringItem) {
//                    print("changed value >> ${dropDownStringItem}");
return DropdownMenuItem<String>(
value: dropDownStringItem,
//this is fro dropdwon value purpose
child: Text(dropDownStringItem),
);
}).toList(),
onChanged: (String value) {
this.selectedProject = value;
//here selected value

setState(() {
taskNames.clear();
for (var i = 0; i < widget.timeSheetObj.entries.length; i++) {
taskNames.add(widget.timeSheetObj.entries[i].task.name);
}
});
},
//if we not mention this property we wont able to see selected text insdie button
//additional properties
iconSize: 40,
iconDisabledColor: Colors.black,
isExpanded: true,
//to fullfill the Dropdownwidth for entire sreen
iconEnabledColor: Colors.blueAccent,
//bottom line for DropDownButton purpose
underline: Container(
decoration: const BoxDecoration(
border: Border(bottom: BorderSide(color: Colors.black))),
),
style: TextStyle(
fontSize: 16,
color: Colors.black,
),
),
DropdownButton<String>(
hint: Text("Select Task"),
value: selectedTask,
isDense: true,
items: taskNames.map((String dropDownStringItem) {
//                    print("changed value >> ${dropDownStringItem}");
return DropdownMenuItem<String>(
value: dropDownStringItem,
//this is fro dropdwon value purpose
child: Text(dropDownStringItem),
);
}).toList(),
onChanged: (String value) {
this.selectedTask = value;
//here selected value
setState(() {
print("selected task is ${this.selectedTask}");
});
},
//if we not mention this property we wont able to see selected text insdie button
//additional properties
iconSize: 40,
iconDisabledColor: Colors.black,
isExpanded: true,
//to fullfill the Dropdownwidth for entire sreen
iconEnabledColor: Colors.blueAccent,
//bottom line for DropDownButton purpose
underline: Container(
decoration: const BoxDecoration(
border: Border(bottom: BorderSide(color: Colors.black))),
),
style: TextStyle(
fontSize: 16,
color: Colors.black,
),
),*/
