import 'model/Timesheet.dart';
import 'model/user.dart';

class Global{

  static User userObject;
  static Timesheet timesheet;
  static List<Timesheet> timeSheetList;

}