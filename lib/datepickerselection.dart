import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';

int _count = 0;
var currencyValues = ["Rupees", "Dhinar", "Dollars"];
FileType _pickingType;
bool _loadingPath = false, _multiPick = false, _hasValidMime = false;
String _fileName, _extension, _path;
Map<String, String> _paths;
TextEditingController _controller = new TextEditingController();

class DatePickerSelection extends StatefulWidget {
  _CustomDatePicker createState() => new _CustomDatePicker();
}

class _CustomDatePicker extends State<DatePickerSelection> {

//  Color gradientStart =
//      Colors.deepPurple[700]; //Change start gradient color here
//  Color gradientEnd = Colors.purple[500]; //Change end gradient color here

  void _valueChangeMethod() {
    setState(() => _count++); //based on setState() value changes dynamically
  }

  @override
  void initState() {
    super.initState();
    _controller.addListener(() => _extension = _controller.text);
  }

  void _openFileExplorer() async {
    if (_pickingType != FileType.CUSTOM || _hasValidMime) {
      setState(() => _loadingPath = true);
      try {
        if (_multiPick) {
          _path = null;
          _paths = await FilePicker.getMultiFilePath(
              type: _pickingType, fileExtension: _extension);
        } else {
          _paths = null;
          _path = await FilePicker.getFilePath(
              type: _pickingType, fileExtension: _extension);
        }
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }
      if (!mounted) return;
      setState(() {
        _loadingPath = false;
        _fileName = _path != null
            ? _path.split('/').last
            : _paths != null ? _paths.keys.toString() : '...';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Hello World App',
        home: new Scaffold(
          appBar: new AppBar(
            title: new Text('Projects'),
            backgroundColor: new Color(0xFF42A5F5),
          ),
          resizeToAvoidBottomPadding: false,
          body: new GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: SingleChildScrollView(
              child: new Container(
                padding: EdgeInsets.only(top: 20),
                child: new Column(
                  children: <Widget>[
//Third for Text Field purpose
                    Padding(
                      padding: EdgeInsets.only(top: 24, left: 12, right: 12),
                      child: new TextField(
//                  textInputAction: TextInputAction.continueAction,

                        keyboardType: TextInputType.number,
//Placeholder purpose
                        decoration: new InputDecoration(
                          border: OutlineInputBorder(),
                          //This is for Border Purpose
                          labelText: "Hours",
                          //float label
                          contentPadding: EdgeInsets.only(
                              left: 4, bottom: 11, top: 11, right: 15),
                          //inside text padding purpose
                          hintText: 'Hours',
                          //Placeholder Label
                          hintStyle: TextStyle(fontSize: 17),
                        ),

                        style: TextStyle(
                          color: Colors.blue,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 24, left: 12, right: 12),
                      child: new TextField(
//                  textInputAction: TextInputAction.continueAction,
                        maxLines: null,
//Placeholder purpose
                        decoration: new InputDecoration(
                          border: OutlineInputBorder(),
                          //This is for Border Purpose
                          labelText: "Comments",
                          //float label
                          contentPadding: EdgeInsets.only(
                              left: 4, bottom: 11, top: 11, right: 15),
                          //inside text padding purpose
                          hintText: 'Comments',
                          //Placeholder Label
                          hintStyle: TextStyle(fontSize: 15),
                        ),

                        style: TextStyle(
                          color: Colors.blue,
                        ),
                      ),
                    ),

                    new Padding(
                      padding:
                          const EdgeInsets.only(top: 20.0, left: 12, right: 12),
                      child: new DropdownButton(
                          iconSize: 40,
                          iconDisabledColor: Colors.orange,
                          isExpanded: true,
                          //to fullfill the Dropdownwidth for entire sreen
                          iconEnabledColor: Colors.blueAccent,
//bottom line for DropDownButton purpose
                          underline: Container(
                            decoration: const BoxDecoration(
                                border: Border(
                                    bottom: BorderSide(color: Colors.grey))),
                          ),
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                          ),
                          hint: new Text('Upload'),
                          value: _pickingType,
                          items: <DropdownMenuItem>[
                            new DropdownMenuItem(
                              child: new Text('FROM AUDIO'),
                              value: FileType.AUDIO,
                            ),
                            new DropdownMenuItem(
                              child: new Text('FROM IMAGE'),
                              value: FileType.IMAGE,
                            ),
                            new DropdownMenuItem(
                              child: new Text('FROM VIDEO'),
                              value: FileType.VIDEO,
                            ),
                            new DropdownMenuItem(
                              child: new Text('FROM ANY'),
                              value: FileType.ANY,
                            ),
                          ],
                          onChanged: (value) => setState(() {
                                _pickingType = value;
                                _openFileExplorer();
                                if (_pickingType != FileType.CUSTOM) {
                                  _controller.text = _extension = '';
                                }
                                print("picking type is ${_pickingType}");
                              })),
                    ),

//TextFormField

                    Padding(
                      padding: EdgeInsets.only(top: 20),
                      child: SizedBox(
                        width: 150,
                        height: 45, // specific value
                        child: RaisedButton(
                          onPressed: () {
                            FocusScope.of(context).requestFocus(
                                FocusNode()); //To hide keyboard purpose
                            //action to be performed//method declaring here
                            print("submit done");
                          },
                          color: Colors.blueAccent,
                          elevation: 23.0,
                          //used to raise 6.0 from background container
                          focusColor: Colors.blue,
                          textColor: Colors.blue,
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.white, fontSize: 30),
                          ),
                        ),
                      ),
                    ),

//Additionally information for file display text purpose

//                new Builder(
//
//                  builder: (BuildContext context) => _loadingPath
//                      ? Padding(
//                      padding: const EdgeInsets.only(bottom: 10.0),
//                      child: const CircularProgressIndicator())
//                      : _path != null || _paths != null
//                      ? new Container(
//
//                    padding: const EdgeInsets.only(bottom: 30.0),
//                    height: MediaQuery.of(context).size.height * 0.50,
//                    child: new Scrollbar(
//                        child: new ListView.separated(
//                          itemCount: _paths != null && _paths.isNotEmpty
//                              ? _paths.length
//                              : 1,
//                          itemBuilder: (BuildContext context, int index) {
//                            final bool isMultiPath =
//                                _paths != null && _paths.isNotEmpty;
//                            final String name = 'File $index: ' +
//                                (isMultiPath
//                                    ? _paths.keys.toList()[index]
//                                    : _fileName ?? '...');
//                            final path = isMultiPath
//                                ? _paths.values.toList()[index].toString()
//                                : _path;
//
//                            return new ListTile(
//                              title: new Text(
//
//                                name,
//
//                              ),
//                              subtitle: new Text(path),
//                            );
//                          },
//                          separatorBuilder:
//                              (BuildContext context, int index) =>
//                          new Divider(),
//                        )),
//                  )
//                      : new Container(),
//                ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
