import 'package:hrms_ui_app/model/UserData.dart';

class TimesheetByUser {

 final int id;
 final String desc;
 final String attachment;
 final String stDate;
 final String endDate;

 final int billableEffort;
 final int nonBillableEffort;

 final UserDta user;
 final Status status;

 TimesheetByUser(
      {this.id, this.desc, this.attachment, this.stDate, this.endDate,this.billableEffort, this.nonBillableEffort,this.user,this.status});

  factory TimesheetByUser.fromJson(Map<String, dynamic> json) {
    return TimesheetByUser(
      id: json['id'],
      desc: json['desc'] ,
      stDate: json['stDate'],
      endDate: json['endDate'],

      billableEffort: json['billableEffort'],
      nonBillableEffort: json['nonBillableEffort'],
      user: UserDta.fromJson(
        json['user'],
      ),
      status: Status.fromJson(
        json['status'],
      ),
    );
  }

}

class UserDta {
 final int id;
 final String token;
 final String firstName;
 final String lastName;
 final String mobile;
 final String email;
 final Type type;

 UserDta({this.id,this.token,this.firstName,this.lastName, this.mobile,this.email,this.type});

  factory UserDta.fromJson(Map<String, dynamic> json) {
    return UserDta(
        id: json['id'],
        token: json['token'] ,
        firstName: json['firstName'],
        lastName: json['lastName'],

        mobile: json['mobile'],
        email: json['email'],

        type: Type.fromJson(
          json['type'],
        ));
  }
}
class Type {
  final int id;
  final String type;

  Type({this.id, this.type});

  factory Type.fromJson(Map<String, dynamic> json) {
    return Type(
      id: json['id'],
      type: json['type'],
    );
  }
}

class Status {
  final int id;
  final String name;
  final String description;

  Status({this.id,this.name,this.description});

  factory Status.fromJson(Map<String, dynamic> json) {
    return Status(
        id: json['id'] ,
        name: json['name'] ,
        description: json['description'] ,
    );
  }

}
