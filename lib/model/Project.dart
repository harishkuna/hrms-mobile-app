class Project {
  int id;
  String name;
  String locationType;

  Project({this.id, this.name, this.locationType});

  Project.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    locationType = json['locationType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['locationType'] = this.locationType;
    return data;
  }
}