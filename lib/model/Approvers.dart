import 'package:hrms_ui_app/model/UserData.dart';

import 'Status.dart';

class Approvers {
  int id;
  Null timesheet;
  UserData approver;
  Status status;

  Approvers({this.id, this.timesheet, this.approver, this.status});

  Approvers.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    timesheet = json['timesheet'];
    approver =
    json['approver'] != null ? new UserData.fromJson(json['approver']) : null;
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['timesheet'] = this.timesheet;
    if (this.approver != null) {
      data['approver'] = this.approver.toJson();
    }
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    return data;
  }
}