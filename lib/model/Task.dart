import 'Project.dart';

class Task {
  int id;
  String name;
  String desc;
  Project project;

  Task({this.id, this.name, this.desc, this.project});

  Task.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    desc = json['desc'];
    project =
    json['project'] != null ? new Project.fromJson(json['project']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['desc'] = this.desc;
    if (this.project != null) {
      data['project'] = this.project.toJson();
    }
    return data;
  }
}