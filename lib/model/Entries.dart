import 'Activity.dart';
import 'EffortType.dart';
import 'Task.dart';

class Entries {
  int id;
  String desc;
  Task task;
  Activity activity;
  EffortType effortType;
  int day1;
  int day2;
  int day3;
  int day4;
  int day5;
  int day6;
  int day7;

  Entries(
      {this.id,
        this.desc,
        this.task,
        this.activity,
        this.effortType,
        this.day1,
        this.day2,
        this.day3,
        this.day4,
        this.day5,
        this.day6,
        this.day7});

  Entries.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    desc = json['desc'];
    task = json['task'] != null ? new Task.fromJson(json['task']) : null;
    activity = json['activity'] != null
        ? new Activity.fromJson(json['activity'])
        : null;
    effortType = json['effortType'] != null
        ? new EffortType.fromJson(json['effortType'])
        : null;
    day1 = json['day1'];
    day2 = json['day2'];
    day3 = json['day3'];
    day4 = json['day4'];
    day5 = json['day5'];
    day6 = json['day6'];
    day7 = json['day7'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['desc'] = this.desc;
    if (this.task != null) {
      data['task'] = this.task.toJson();
    }
    if (this.activity != null) {
      data['activity'] = this.activity.toJson();
    }
    if (this.effortType != null) {
      data['effortType'] = this.effortType.toJson();
    }
    data['day1'] = this.day1;
    data['day2'] = this.day2;
    data['day3'] = this.day3;
    data['day4'] = this.day4;
    data['day5'] = this.day5;
    data['day6'] = this.day6;
    data['day7'] = this.day7;
    return data;
  }
}