import 'package:hrms_ui_app/model/Type.dart';

class UserData {
  int id;
  String token;
  String firstName;
  String lastName;
  String mobile;
  String email;
  Type type;

  UserData(
      {this.id,
        this.token,
        this.firstName,
        this.lastName,
        this.mobile,
        this.email,
        this.type});

  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    token = json['token'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    mobile = json['mobile'];
    email = json['email'];
    type = json['type'] != null ? new Type.fromJson(json['type']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['token'] = this.token;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['mobile'] = this.mobile;
    data['email'] = this.email;
    if (this.type != null) {
      data['type'] = this.type.toJson();
    }
    return data;
  }
}