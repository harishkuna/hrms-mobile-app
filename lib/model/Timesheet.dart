import 'package:hrms_ui_app/model/UserData.dart';

import 'Approvers.dart';
import 'Entries.dart';
import 'Status.dart';

class Timesheet {
  int id;
  String desc;
  UserData user;
  String attachment;
  String stDate;
  String endDate;
  Status status;
  int billableEffort;
  int nonBillableEffort;
  List<Entries> entries;
  List<Approvers> approvers;

  Timesheet(
      {this.id,
        this.desc,
        this.user,
        this.attachment,
        this.stDate,
        this.endDate,
        this.status,
        this.billableEffort,
        this.nonBillableEffort,
        this.entries,
        this.approvers});

  Timesheet.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    desc = json['desc'];
    user = json['user'] != null ? new UserData.fromJson(json['user']) : null;
    attachment = json['attachment'];
    stDate = json['stDate'];
    endDate = json['endDate'];
    status =
    json['status'] != null ? new Status.fromJson(json['status']) : null;
    billableEffort = json['billableEffort'];
    nonBillableEffort = json['nonBillableEffort'];
    if (json['entries'] != null) {
      entries = new List<Entries>();
      json['entries'].forEach((v) {
        entries.add(new Entries.fromJson(v));
      });
    }
    if (json['approvers'] != null) {
      approvers = new List<Approvers>();
      json['approvers'].forEach((v) {
        approvers.add(new Approvers.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['desc'] = this.desc;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['attachment'] = this.attachment;
    data['stDate'] = this.stDate;
    data['endDate'] = this.endDate;
    if (this.status != null) {
      data['status'] = this.status.toJson();
    }
    data['billableEffort'] = this.billableEffort;
    data['nonBillableEffort'] = this.nonBillableEffort;
    if (this.entries != null) {
      data['entries'] = this.entries.map((v) => v.toJson()).toList();
    }
    if (this.approvers != null) {
      data['approvers'] = this.approvers.map((v) => v.toJson()).toList();
    }
    return data;
  }
}