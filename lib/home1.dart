import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hrms_ui_app/Global.dart';
import 'package:hrms_ui_app/model/TimeSheetByUser.dart';
import 'package:hrms_ui_app/model/Timesheet.dart';
import 'package:hrms_ui_app/task.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

//void main() => runApp(new HomeView());
String startDateVal = "Start Date", endDateVal = "End Date", hoursVal = "";
List<dynamic> timeSheetList;

class HomeView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return MainPage();
  }
}

class MainPage extends State<HomeView> {
  @override
  void initState() {
    fetchTime();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            title: Text('TimeSheet'),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, false),
            )),
        body: new ListView.builder(
          itemCount: timeSheetList == null ? 0 : timeSheetList.length,
          itemBuilder: (context, index) {
            Timesheet timeSheetObj =
                new Timesheet.fromJson(timeSheetList[index]);

            DateFormat output = DateFormat("yyyy/MM/dd");

            return Card(
              child: ListTile(
                title: Text(
                    "${output.format(DateTime.parse(timeSheetObj.stDate))} - ${output.format(DateTime.parse(timeSheetObj.endDate))}"),
                subtitle: Text(timeSheetObj.status.name),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TaskPage(timeSheetObj)),
                  );
                },
              ),
            );
          },
        ));
  }

//fetchTime Api call
  fetchTime() async {
    String basicAuth = 'Bearer ' + Global.userObject.accessToken;
    print(basicAuth);

    Map<String, String> headers = {
      'Content-Type': 'application/json',
      HttpHeaders.AUTHORIZATION: basicAuth,
    };

    final response = await http
        .get('http://192.168.10.227:8080/timesheet/user/2', headers: headers);

    print("response code >> + ${response.statusCode}");

    print("response  >> + ${response.body}");

    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);
      print("response  >> + ${responseJson}");
      setState(() {
        timeSheetList = json.decode(response.body);
        print("timesheet 0  >> + ${timeSheetList}");
      });
    }
  }
}
